//
//  ViewController.swift
//  Number
//
//  Created by Sergey Kim on 11.08.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

import UIKit
import NumberConverter

final class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var items: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44

        self.items = self.readPlist()
        self.tableView.reloadData()
    }

    private func readPlist(name: String = "input") -> [String] {
        var valuesDictionary: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource(name, ofType: "plist") {
            valuesDictionary = NSDictionary(contentsOfFile: path)
        }
        guard let dict = valuesDictionary else {
            return []
        }

        var resultItems = [String]()
        for key in dict.allKeys {
            if let strKey = key as? String,
                let _ = Int(strKey) {
                resultItems.append(strKey)
            }
        }

        return resultItems
    }

    @IBAction func switchBarButtonSelected(sender: AnyObject) {
        self.items = self.items.map { NumberConverter.convert(number: Int($0)! ) }
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: TextTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TextTableViewCell

        cell.titleLabel.text = items[indexPath.row]

        return cell
    }
}

extension ViewController: UITableViewDelegate {

}
