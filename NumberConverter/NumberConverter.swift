//
//  NumberConverter.swift
//  Number
//
//  Created by Sergey Kim on 11.08.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

import Foundation

public class NumberConverter {
    public static func convert(number number: Int) -> String {

        if number == 0 {
            return "ноль"
        }

        var numberString = String(number)
        var resultString = ""

        var index = 0
        while numberString.characters.count > 0 {
            let triade = numberString.right(3)

            resultString = NumberConverter.convertTriade(index, numberString: triade) + " " + resultString
            resultString = resultString.trim()
            index = index + 1

            numberString.removeRight(3)
        }

        return resultString
    }

    private static func convertTriade(triadeIndex: Int, numberString: String) -> String {
        var result = [String](count: 4, repeatedValue: "")

        var number = numberString
        let digit    = Int(number.removeRight(1))
        let tens     = Int(number.removeRight(1)) ?? 0
        let hundreds = Int(number.removeRight(1)) ?? 0

        guard let wholeNumber = Int(numberString) where wholeNumber > 0 else {
            return ""
        }

        if hundreds > 0 {
            result[0] = NumberDescription.hundForNumber(hundreds)
        }
        if tens == 1 {
            result[1] = NumberDescription.wordForNumber(digit! + tens * 10, triadeIndex: triadeIndex)
        } else {
            result[1] = NumberDescription.tenForNumber(tens)
            result[2] = NumberDescription.wordForNumber(digit!, triadeIndex: triadeIndex)
        }

        result[3] = NumberDescription.triadeForNumber(triadeIndex, number: digit!)
        let returnValue = result.filter { $0.characters.count > 0 }.joinWithSeparator(" ")
        return returnValue
    }
}
