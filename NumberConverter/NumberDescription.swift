//
//  NumberDescription.swift
//  Number
//
//  Created by Sergey Kim on 15.08.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

import Foundation

enum Triads {
    case thousand(Int)
    case million(Int)
    case billion(Int)
    case trillion(Int)

    static let allValues = [thousand, million, billion, trillion]

    var description: String {
        switch self {
        case .thousand (let num):
            if num == 1 {
                return "тысяча"
            }
            if num > 1 && num < 5 {
                return "тысячи"
            }
            return "тысяч"
        case .million (let num):
            if num == 1 {
                return "миллион"
            }
            if num > 1 && num < 5 {
                return "миллиона"
            }
            return "миллионов"
        case .billion (let num):
            if num == 1 {
                return "миллиард"
            }
            if num > 1 && num < 5 {
                return "миллиарда"
            }
            return "миллиардов"
        case .trillion (let num):
            if num == 1 {
                return "триллион"
            }
            if num > 1 && num < 5 {
                return "триллиона"
            }
            return "триллионов"
        }
    }

    var gender: Int {
        switch self {
        case .thousand:
            return 1
        default : return 0
        }
    }
}

enum Words {
    case one(Int)
    case two(Int)
    case three(Int)
    case four(Int)
    case five(Int)
    case six(Int)
    case seven(Int)
    case eight(Int)
    case nine(Int)
    case ten(Int)
    case eleven(Int)
    case twelve(Int)
    case thirteen(Int)
    case fourteen(Int)
    case fifteen(Int)
    case sixteen(Int)
    case seventeen(Int)
    case eighteen(Int)
    case nineteen(Int)

    static let allValues = [one,
                            two,
                            three,
                            four,
                            five,
                            six,
                            seven,
                            eight,
                            nine,
                            ten,
                            eleven,
                            twelve,
                            thirteen,
                            fourteen,
                            fifteen,
                            sixteen,
                            seventeen,
                            eighteen,
                            nineteen]

    var description: String {
        switch self {
        case .one (let num):
            if num == 1 {
                return "одна"
            }
            return "один"
        case .two (let num):
            if num == 1 {
                return "две"
            }
            return "два"
        case .three:
            return "три"
        case .four:
            return "четыре"
        case .five:
            return "пять"
        case .six:
            return "шесть"
        case .seven:
            return "семь"
        case .eight:
            return "восемь"
        case .nine:
            return "девять"
        case .ten:
            return "десять"
        case .eleven:
            return "одиннадцать"
        case .twelve:
            return "двенадцать"
        case .thirteen:
            return "тринадцать"
        case .fourteen:
            return "четырнадцать"
        case .fifteen:
            return "пятнадцать"
        case .sixteen:
            return "шестнадцать"
        case .seventeen:
            return "семнадцать"
        case .eighteen:
            return "восемнадцать"
        case .nineteen:
            return "девятнадцать"
        }
    }
}

final class NumberDescription {

    private static var tens = [ "",
                                "десять",
                                "двадцать",
                                "тридцать",
                                "сорок",
                                "пятьдесят",
                                "шестьдесят",
                                "семьдесят",
                                "восемьдесят",
                                "девяносто",
                                ]

    private static var hundreds = [ "",
                                    "сто",
                                    "двести",
                                    "триста",
                                    "четыреста",
                                    "пятьсот",
                                    "шестьсот",
                                    "семьсот",
                                    "восемьсот",
                                    "девятьсот",
                                    ]

    static func wordForNumber(number: Int, triadeIndex: Int) -> String {
        var gender = 0
        if triadeIndex > 0 {
            let triade = Triads.allValues[triadeIndex-1](0)
            gender = triade.gender
        }

        guard number > 0 else {
            return ""
        }

        let word = Words.allValues[number-1](gender)
        return word.description
    }

    static func tenForNumber(number: Int) -> String {
        return NumberDescription.tens[min(number, NumberDescription.tens.count-1)]
    }

    static func hundForNumber(number: Int) -> String {
        return NumberDescription.hundreds[min(number, NumberDescription.hundreds.count-1)]
    }

    static func triadeForNumber(index: Int, number: Int) -> String {
        guard index > 0 else {
            return ""
        }
        let triade = Triads.allValues[index-1](number)

        return triade.description
    }
}
