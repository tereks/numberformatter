//
//  String+Index.swift
//  Number
//
//  Created by Sergey Kim on 15.08.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

import Foundation

extension String {
    func left(count: Int) -> String {
        let index = self.startIndex.advancedBy(min(count, self.characters.count))

        let substring = self.substringToIndex(index)
        return substring
    }

    func right(count: Int) -> String {
        let index = self.endIndex.advancedBy(-min(count, self.characters.count))

        let substring = self.substringFromIndex(index)
        return substring
    }

    func mid(start: Int, count: Int) -> String {
        let startIndex = self.startIndex.advancedBy(min(start, self.characters.count))
        let charsCount = min(count, self.characters.count - startIndex.distanceTo(self.endIndex) + 1)

        return self.substringWithRange(Range<String.Index>(start: startIndex, end: startIndex.advancedBy(charsCount)))
    }

    mutating func removeRight(count: Int) -> String {
        let index = self.endIndex.advancedBy(-min(count, self.characters.count))
        let truncated = self.substringToIndex(index)
        let right = self.substringFromIndex(index)
        self = truncated

        return right
    }
}

extension String {
    func trim() -> String {
        return self.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
    }
}